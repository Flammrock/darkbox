@Echo off
if defined __ goto :expr
set __=.
call %0 %* | darkbox
set __=
pause>nul
goto :eof

:expr
setlocal enabledelayedexpansion


call :_element 10 10


echo -q
goto :EOF
 
REM ┌──────────┐
REM │          │
REM └──────────┘
:element X Y
   set /a "element_x=%~1"
   for /l %%i in (0,1,3) do set /a "element_y.%%i=%~2 + %%i"
   
   echo -g !element_x! !element_y.0! -12a 218 196 196 196 196 196 196 196 196 196 196 191 
   echo -g !element_x! !element_y.1! -12a 179 32 32 32 32 32 32 32 32 32 32 179 
   echo -g !element_x! !element_y.2! -12a 192 196 196 196 196 196 196 196 196 196 196 217
   
   set "element_x="
   for /l %%i in (0,1,3) do set "element_y.%%i="
exit/b

	
:_element X Y
   echo -g %~1 %~2+0 -12a 218 196 196 196 196 196 196 196 196 196 196 191 
   echo -g %~1 %~2+1 -12a 179 32 32 32 32 32 32 32 32 32 32 179 
   echo -g %~1 %~2+2 -12a 192 196 196 196 196 196 196 196 196 196 196 217
exit/b